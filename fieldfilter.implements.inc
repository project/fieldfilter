<?php
/**
 * @file
 * API implementation on behalf of other modules.
 */

/**
 * Implements hook_fieldfilter_display_alter() on behalf of Image module.
 */
function image_fieldfilter_display_alter(&$display, &$result) {
  if ($result['type'] == 'image') {
    $image_styles = image_styles();

    if (isset($result['settings'])) {
      if (isset($result['settings']['size'])) {
        $size = $result['settings']['size'];
        $class[] = 'style-' . $size;
        // Checks if the selected image size exists in the system and replaces the default display with it
        if (isset($image_styles[$size]) && is_array($image_styles[$size])) {
          $display['settings']['image_style'] = $size;
        }
      }
    }
    else {
      $result['settings'] = array();
    }
  }
}

/**
 * Implements hook_fieldfilter_display_alter() on behalf of Video Embed Field module.
 */
function field_collection_fieldfilter_display_alter(&$display, &$result) {
  if ($result['type'] == 'field_collection') {
    $result['field_container'] = 'div';
  }
}
