<?php
/**
 * @file
 * Main functions for Field Filter.
 */

/**
 * Implements hook_help().
 */
function fieldfilter_help($path = 'admin/help#fieldfilter', $arg) {
  switch ($path) {
    case 'admin/help#fieldfilter':
      return '<p>' . t("The Field Filter allows you to enter placeholders for fields, which will be displayed using the entity’s default field-display settings. Basic display parameters can also be specified.") . '</p>';
      break;
  }
}

/**
 * Implements hook_filter().
 */
function fieldfilter_filter_info() {
  $filters['fieldfilter'] = array(
    'title' => t('Field Filter'),
    'description' => t("Allows entering placeholders for fields, which will be displayed using the entity’s default field-display settings. Basic display parameters can also be specified."),
    'process callback' => '_fieldfilter_process',
  );
  return $filters;
}

/**
 * Filter process callback.
 */
function _fieldfilter_process($text, $format) {
  if (!empty($text)) {
    $filter = _fieldfilter_filter_match($text);

    if (count($filter)) {
      foreach ($filter[0] as $field) {
        // Replace brackets with HTML comment
        $newfield = str_ireplace("[", "<!-- ", $field);
        $newfield = str_ireplace("]", " -->", $newfield);
        $text = str_ireplace($field, $newfield, $text);
      }

      return $text;
    }
    else {
      return $text;
    }
  }
}

/**
 * Implements hook_entity_view_alter().
 */
function fieldfilter_entity_view_alter(&$build, $type) {
  $build['#post_render'][] = '_fieldfilter_post_render';
}

/**
 * Entity post-render callback.
 */
function _fieldfilter_post_render($content, $element) {
  $entity_type = $element['#entity_type'];
  $bundle = $element['#bundle'];

  // Default entities aren't consistent in their naming
  // Guess based on entity type. Works for node, comment
  $entity_key = "#{$entity_type}";

  if (!isset($element[$entity_key])) {
    switch ($entity_type) {
      case 'user' :
        $entity_key = '#account';
      break;

      case 'taxonomy_term' :
        $entity_key = '#term';
      break;

      // Covers entities provided through Entity API
      default :
        $entity_key = '#entity';
    }
  }

  // Do nothing if we haven't found the proper entity
  if (!isset($element[$entity_key])) {
    return $content;
  }

  $entity = $element[$entity_key];
  $matches = _fieldfilter_post_render_match($content);

  // Do nothing if there is no field filter
  if (empty($matches[0])) {
    return $content;
  }

  foreach ($matches[0] as $delta => $string) {
    $results[$delta] = array(
      'match' => $string,
      'field' => $matches['field'][$delta],
      // Assign proper delta
      'field_delta' => $matches['field_delta'][$delta] > 0 ? $matches['field_delta'][$delta] - 1 : 0,
    );

    $results[$delta]['settings'] = array();
    if (strlen($matches['settings'][$delta]) > 1) {
      $settings = explode("|", $matches['settings'][$delta]);

      foreach ($settings as $setting) {
        $setting = explode(":", trim($setting));
        // Set value = key if no value was provided
        $results[$delta]['settings'][trim($setting[0])] = !empty($setting[1]) ? trim($setting[1]) : trim($setting[0]);
      }
    }
  }

  $entity_fields = array_keys((array) $entity);

  foreach ($results as $result) {
    // Test field name for custom/default fields
    $field = preg_grep("/(field_)?" . $result['field'] . "/", $entity_fields);

    // Default replacement for no matches or processing errors
    $replacement = '';

    if (!empty($field)) {
      $field_name_short = $result['field'];      // Will be used for classes
      // Use only first result. Chances of there being more than one are slim
      $field_name = reset($field);
      $field_delta = $result['field_delta'];

      // Field has no visible values
      if (empty($element[$field_name])) {
        $replacement = '';
      }
      else {
        // Chosen delta doesn't exist
        if (empty($element[$field_name][$field_delta])) {
          $replacement = '';
        }
        else {
          $field = $element[$field_name];
          $type = $field['#field_type'];
          $instance = field_info_instance($entity_type, $field_name, $bundle);
          $result['type'] = $type;

          !empty($result['settings']['align']) ? $align = explode(" ", $result['settings']['align']) : $align = array();
          !empty($result['settings']['class']) ? $class = explode(" ", $result['settings']['class']) : $class = array();

          $class = array_merge(array('fieldfilter', $field_name_short), $align, $class);

          $view_mode = $field['#view_mode'] == 'full' ? 'default' : $field['#view_mode'];
          $display = !empty($instance['display'][$view_mode]) ? $instance['display'][$view_mode] : $instance['display']['default'];

          drupal_alter('fieldfilter_display', $display, $result);

          $language = field_language($entity_type, $entity, $field_name);

          if (!empty($entity->{$field_name}[$language])) {
            $field = $entity->{$field_name}[$language];
            $item = $field[$field_delta];

            if (!empty($item)) {
              $fielddata = field_view_value($entity_type, $entity, $field_name, $item, $display, $language);
              $container = !empty($result['field_container']) ? $result['field_container'] : 'span';
              $replacement = theme('fieldfilter', array('field_type' => $type, 'field_data' => $fielddata, 'field_class' => $class, 'field_container' => $container, 'field_settings' => $result['settings']));
            }
          }
        }
      }
    }

    $content = str_replace($result['match'], $replacement, $content);
    drupal_add_css(drupal_get_path('module', 'fieldfilter') .'/fieldfilter.css');
  }

  return $content;
}

/**
 * Matches user-entered Field Filter syntax in text.
 */
function _fieldfilter_filter_match($content) {
  // Match syntax like [field:n], where n is an integer
  // This may interfere with other filters. Needs more specificity
  preg_match_all("/\[\w+:\d*\|?.*\]/", $content, $matches);

  return $matches;
}

/**
 * Matches processed Field Filter syntax in text.
 */
function _fieldfilter_post_render_match($content) {
  preg_match_all("/\<!\-\-\s*" . "(?'field'\w+):(?'field_delta'\d*)\|?(?'settings'.*)" . "\s*\-\-\>/", $content, $matches);

  return $matches;
}

/**
 * Outputs HTML for fields rendered through Field Filter.
 */
function theme_fieldfilter($variables) {
  $output = drupal_render($variables['field_data']);
  $container = $variables['field_container'];

  // Remove width and height from images to promote fluidity. Will have to be moved to own filter/function.
  if ($variables['field_type'] == 'image') {
    $output = preg_replace("/\s*(width|height)=\"\d*\"/", "", $output, 2);
    $variables['field_class'][] = 'style-' . $variables['field_data']['#image_style'];
  }

  if (count($variables['field_class'])) {
    $class = " class=\"" . implode(" ", $variables['field_class']) . "\"";
  }

  $output = "<{$container}$class>\n{$output}\n";

  if (!empty($variables['field_data']['#item']['title']) && !isset($variables['field_settings']['notitle'])) {
    $output .= "<span class=\"field caption\">" . $variables['field_data']['#item']['title'] . "</span>\n";
  }
  $output .= "</$container>\n";

  return $output;
}

/**
 * Implements hook_theme().
 */
function fieldfilter_theme() {
  return array(
    'fieldfilter' => array(
      'variables' => array(
        'field_type' => NULL,
        'field_data' => array(),
        'field_class' => array(),
        'field_container' => NULL,
        'field_settings' => array(),
      ),
    ),
  );
}

module_load_include('inc', 'fieldfilter', 'fieldfilter.implements');
