<?php
/**
 * @file
 * Field Filter API hooks.
 */

/**
 * Allows modules to change the display structure for a single field.
 */
function hook_fieldfilter_field_display_alter(&$settings) {
  // Example from image_fieldfilter_display_alter()
  if ($result['type'] == 'image') {
    $image_styles = image_styles();

    if (isset($result['settings'])) {
      if (isset($result['settings']['size'])) {
        $size = $result['settings']['size'];
        $class[] = 'style-' . $size;
        // Checks if the selected image size exists in the system and replaces the default display with it
        if (is_array($image_styles[$size])) {
          $display['settings']['image_style'] = $size;
        }
      }
    }
    else {
      $result['settings'] = array();
    }
  }
}
