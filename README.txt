# Field Filter module

## Notice

This module replaces [File Filter][filefilter] by allowing content authors
to add to their formatted text areas placeholders to display values of fields
attached to the current entity.

While File Filter worked only for file fields attached to nodes, Field Filter
works for any field (whether custom or module-provided) attached to any entity.
It also provides a basic API for other modules to extend its functionality.
Support for the Image module, migrated from File Filter, is included.

[filefilter]: https://drupal.org/project/filefilter

## Installation

Download and enable the Field Filter module, then enable the filter in one
or more of your text formats at `admin/config/content/formats`.

## Usage

### Setting up your template
Once you've set up a field (for example, `file_image`) for a certain
content type, you must make sure that:

1. the field is visible in the field-display settings for your desired content
type
2. the field is hidden at the template level.

To hide your field in your template, edit node.tpl.php in your theme (or the
specific template file for your content type or other entity) and add, at the
top of the file:

    <?php
    hide($content['file_image']);
    ?>

Of course you'll have to use the specific name of your field.

If you don't hide your field in the template, it will appear both in the
specified spot in the body and wherever the file is set to appear by the field's
display settings for your content type. On the other hand, if you hide the field
in the display settings, it will not appear at all.

### Filter syntax

The easiest way to use the Field Filter is by using this syntax in the node body,
in the spot where you'd like your file to be displayed.

    [image:1]

If you're adding a custom field, you can enter the field name with or without
the initial `field_`. You must also use progressive numbers (starting from 1)
for the different values in multiple-value fields.

As another example, if the field you set up is `field_coverimage`, and you want
to display the fourth value in the field, the correct syntax will be

    [coverimage:4]

Using the basic syntax, the file will be displayed according to the field's
display settings for the specific content type.

### Using image styles

In the case of image fields, it's possible to specify a different image style
from the default one provided in the field's display settings. If you want to
display the third `file_image` value using the `large` style, simply use this
syntax:

    [image:3|size:large]

If the specific image style isn't found, the image will be displayed in the
default style for that content type.

### More Field Filter attributes

Note that attributes can be entered in any order.

#### No title

While normally images are displayed with the title attribute (if present) used
as caption, you can choose not to display a caption by adding a `notitle`
attribute to the syntax:

    [image:3|size:large|notitle]

#### Alignment

The Field Filter module comes with a few preset styles (that can be overridden in
your theme) to simplify image alignment. The `align` attribute defaults to
`left`, and can also take `center` and `right` as possible values:

    [image:3|size:large|align:right]

#### Extra classes

You can also specify custom classes using the `class` attribute. Multiple classes
must be separated with a space.